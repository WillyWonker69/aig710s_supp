### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ f6c1c5e0-ddb7-11eb-3607-ff408fdf4b39
begin
	using Pkg
	Pkg.activate("Project.toml")
	using PlutoUI
end

# ╔═╡ be8dda01-1a52-4283-97d9-067bbf0e5bd7
using Printf

# ╔═╡ 5d5d2a16-7fed-4090-b901-ff4bf9476d85
using UnPack

# ╔═╡ 4c66368c-6f13-4829-a876-abb0999d313b
using ProgressMeter: @showprogress

# ╔═╡ 823a8757-660d-4418-b627-0d6a33f572a0
using Evolutionary

# ╔═╡ 226c1841-7b51-4b08-9c81-70a99dcc1152
using LinearAlgebra

# ╔═╡ 612c20ee-1292-4bba-94f7-a8a882dd22da
using NLSolversBase

# ╔═╡ ef371103-bf89-483f-9574-efeeb1db295e
using Statistics

# ╔═╡ 02545ddc-8723-4fdc-b1e7-af5adf8525b6
using Test

# ╔═╡ 8cbb1258-d1a1-40a3-a1dc-fd5c5a5ab085
using Random

# ╔═╡ d68f7f21-eca9-44ec-9372-405be9affa2f
Random.seed!(2);

# ╔═╡ 8e831c84-2ec1-4882-8a1d-5c6fda0abfc7
md"# GA - Genetic Algorithms
## Evolutionary Branch"

# ╔═╡ a032688a-a209-499d-9951-eab27feca15b
md"## Defining The Genetic Algorithm"

# ╔═╡ b4c44fb9-0fd9-4a2c-87ea-60f3104612f3
 mutable struct GA <: Evolutionary.AbstractOptimizer
    populationSize::Int 	 	#Default = 100
    crossoverRate::Float64 	 	#Default = 0.8 (80%)
    mutationRate::Float64 	 	#Default = 0.1 (10%)
    ɛ::Real 	 				#Default = 0 (How many in the population will live)
	
    selection::Function  		#Selection Function
    crossover::Function 		#crossover Function
    mutation::Function 			#Mutation Function

	#Genetic Algorithm defaul value instansiation
GA(; populationSize::Int=100, crossoverRate::Float64=0.5, mutationRate::Float64=0.1,
        ɛ::Real=0, epsilon::Real=ɛ,
        selection::Function = ((x,n)->1:n),
        crossover::Function = identity, mutation::Function = identity) =
new(populationSize,crossoverRate,mutationRate,epsilon,selection,crossover, mutation)
	
	 Evolutionary.population_size(method::GA) = method.populationSize
	 Evolutionary.default_options(method::GA) = (iterations=1000, abstol=1e-15)

summary(m::GA) = "Wonkers_GA[P=$(m.populationSize),x=$(m.crossoverRate),μ=$(m.mutationRate),ɛ=$(m.ɛ)]"
	
	printf(io::IO,m::GA) = print(io, summary(m))
	
end

# ╔═╡ 60106ec6-9b12-457a-8e69-c95a3c4d8cb6
md"## Genetic Algorithm Utitliy Functions"

# ╔═╡ faf0eb22-603a-49d1-9561-a6f1eba68fab
mutable struct GAState{T,IT} <: Evolutionary.AbstractOptimizerState
    N::Int   				#Number of Elements
    eliteSize::Int 			#Size of Elite Children in pop
    fitness::T 				#Fitness function 
    fitpop::Vector{T} 		
    fittest::IT
end

# ╔═╡ 69251b21-feb8-40c8-b716-23b434f6e439
md"## Fitness Function Minimization"

# ╔═╡ ed694309-def5-4af3-826c-34da628e9b85
begin
	Evolutionary.value(s::GAState) = s.fitness
	Evolutionary.minimizer(s::GAState) = s.fittest #Returns minmizer object in c state
end

# ╔═╡ f0e5cb0e-45f6-46a9-9214-f847255a6212
md"## Genetic Algorithm  Initialization "

# ╔═╡ 80388176-a6f1-497b-b0a9-b3d926d07879

function Evolutionary.initial_state(method::GA, options, objfun, population)
    T = typeof(Evolutionary.value(objfun)) #Get Element Type
    N = length(first(population)) #First Element in the population
    fitness = zeros(T, method.populationSize) #Create a matrix of zeros to be filled

    # setup state values
	
	#isa(method.ɛ, Int) (is e an int, if so isa = true
	#if true, return method.ɛ as and Int
	#else compute pop size using => round(Int, method.ɛ * method.populationSize) 
	
eliteSize = isa(method.ɛ, Int) ? method.ɛ : round(Int,method.ɛ * method.populationSize)

    # Evaluate population fitness
    fitness = map(i -> value(objfun, i), population)
    minfit, index = findmin(fitness)

    # setup initial state
    return GAState(N, eliteSize, minfit, fitness, copy(population[index]))
end

# ╔═╡ ebf5b893-a9f4-4271-9c69-ab25b0aaa354
md"## Genetic Algorithm State Update"

# ╔═╡ f73f8a69-35a3-4b96-98e9-3a3cea125342
function Evolutionary.update_state!(objfun, constraints, state, population::AbstractVector{IT}, method::GA, itr) where {IT}
    @unpack populationSize,crossoverRate,mutationRate,ɛ,selection,crossover,mutation = method

    offspring = similar(population)

    # Select offspring
	
	#Selection of the fittest from the population
    selected = selection(state.fitpop, populationSize) 

    # Perform mating
    offidx = randperm(populationSize) #Generate a random ?
	
	 #new population Size (without elite offspring)
    offspringSize = populationSize - state.eliteSize
    
	for i in 1:2:offspringSize
        j = (i == offspringSize) ? i-1 : i+1
        if rand() < crossoverRate
            offspring[i], offspring[j] = crossover(population[selected[offidx[i]]], population[selected[offidx[j]]])
        else
            offspring[i], offspring[j] = population[selected[i]], population[selected[j]]
        end
    end

    # Elitism
	#Choosing the Best Solution
    fitidxs = sortperm(state.fitpop)
    for i in 1:state.eliteSize
        subs = offspringSize+i
        offspring[subs] = copy(population[fitidxs[i]])
    end

    # Performing our  mutation
    for i in 1:offspringSize
        if rand() < mutationRate
            mutation(offspring[i])
        end
    end

    # Creat a new generation and evaluating
    for i in 1:populationSize
        o = apply!(constraints, offspring[i])
        population[i] = o
        state.fitpop[i] = value(objfun, o)
    end
    
	
	#penalize  solutions that arent Feasible
    penalty!(state.fitpop, constraints, population)

    # finding the best individual
    minfit, fitidx = findmin(state.fitpop)
    state.fittest = population[fitidx]
    state.fitness = state.fitpop[fitidx]

    return false
end

# ╔═╡ 1ec0cade-d3fa-488b-8ddf-4271ffd1ebb2
md"# Cross Over Function"

# ╔═╡ 59fa7dbd-6e81-4111-8974-dbdc1c896bd8
md"## Partially mapped crossover"

# ╔═╡ 5066dff5-c2c1-4675-af5b-184455cf7e64
function PMX(v1::T, v2::T) where {T <: AbstractVector}
	
    s = length(v1)
	
    from, to = rand(1:s, 2) #Choose a random Crosover point
	
	#Check which is greater and swap if need be
    from, to = from > to ? (to, from)  : (from, to) 
    c1 = similar(v1)
    c2 = similar(v2)

    # Swap the sections 
    c1[from:to] = v2[from:to]
    c2[from:to] = v1[from:to]

    # Fill in from parents 
    for i in vcat(1:from-1, to+1:s)
        # Check for conflicting offspring
        in1 = inmap(v1[i], c1, from, to)
        if in1 == 0
            c1[i] = v1[i]
        else
            tmpin = in1
            while tmpin > 0
                tmpin = inmap(c2[in1], c1, from, to)
                in1 = tmpin > 0 ? tmpin : in1
            end
            c1[i] = v1[in1]
        end

        in2 = inmap(v2[i], c2, from, to)
        if in2 == 0
            c2[i] = v2[i]
        else
            tmpin = in2
            while tmpin > 0
                tmpin = inmap(c1[in2], c2, from, to)
                in2 = tmpin > 0 ? tmpin : in2
            end
            c2[i] = v2[in2]
        end
    end
    return c1, c2
end

# ╔═╡ 47afdf48-0cda-4df5-8fa1-acd18617562b
md"## Order Crossover Operator (OX1)"

# ╔═╡ b41e2a4c-32c1-4890-90cd-9fd2344524c5
function OX1(v1::T, v2::T) where {T <: AbstractVector}
    s = length(v1)
    from, to = rand(1:s, 2)
    from, to = from > to ? (to, from)  : (from, to)
    c1 = zero(v1)
    c2 = zero(v2)
    # Swap
    c1[from:to] = v2[from:to]
    c2[from:to] = v1[from:to]
    # Fill in from parents
    k = to+1 > s ? 1 : to+1 #child1 index
    j = to+1 > s ? 1 : to+1 #child2 index
    for i in vcat(to+1:s,1:from-1)
        while in(v1[k],c1)
            k = k+1 > s ? 1 : k+1
        end
        c1[i] = v1[k]
        while in(v2[j],c2)
            j = j+1 > s ? 1 : j+1
        end
        c2[i] = v2[j]
    end
    return c1, c2
end


# ╔═╡ e522ae8a-d721-4d9f-8132-bb99cfab4636
md"# Mutation Functions 
## (Mutate the Element)"

# ╔═╡ b6d16479-6dc2-488a-b5d7-da4a53a57e8f
md"## Insertion
### Moves a Random value in selected Element to a random position "

# ╔═╡ b3d98fe2-b6e2-4939-9c28-95c6478f797f
function insertion(recombinant::T) where {T <: AbstractVector}
    l = length(recombinant)
    from, to = rand(1:l, 2)
    val = recombinant[from]
    deleteat!(recombinant, from)
    return insert!(recombinant, to, val)
end

# ╔═╡ c0ae0d27-5221-473b-ab66-e9361890a29b
md"## Swap
### Swaps Two Random Values in the Element"

# ╔═╡ b9dc04ab-84fa-4213-83a8-27d866abdbc0
function swap2(recombinant::T) where {T <: AbstractVector}
    l = length(recombinant)
    from, to = rand(1:l, 2)
    swap!(recombinant, from, to)
    return recombinant
end

# ╔═╡ 1e5bb800-fcfd-46fe-b31e-e9da418115b6
md"## Scramble
### Scrambles a random section of the Element"

# ╔═╡ 85ca7d49-85cd-4daa-970a-8f7165187ee4
function scramble(recombinant::T) where {T <: AbstractVector}
    l = length(recombinant)
    from, to = rand(1:l, 2)
    from, to = from > to ? (to, from)  : (from, to)
    diff = to - from + 1
    if diff > 1
        patch = recombinant[from:to]
        idx = randperm(diff)
        for i in 1:diff
            recombinant[from+i-1] = patch[idx[i]]
        end
    end
    return recombinant
end

# ╔═╡ 53aea851-68c5-4fce-861e-fee79e011722
md"## Shifting
### Shifts a Random length of the selected Element"

# ╔═╡ 59c708ad-27e4-4de6-b206-98c4d10cceb7
function shifting(recombinant::T) where {T <: AbstractVector}
    l = length(recombinant)
    from, to, where = sort(rand(1:l, 3))
    patch = recombinant[from:to]
    diff = where - to
    if diff > 0
        # move values after tail of patch to the patch head position
        for i in 1:diff
            recombinant[from+i-1] = recombinant[to+i]
        end
        # place patch values in order
        start = from + diff
        for i in 1:length(patch)
            recombinant[start+i-1] = patch[i]
        end
    end
    return recombinant
end

# ╔═╡ 22f5ed9b-b8ff-4eb5-870b-c5690b24f33c
md"## Inversion
### Inverts a random length of the selected element"

# ╔═╡ e5848996-b623-4122-83c7-bce1059b7854
function inversion(recombinant::T) where {T <: AbstractVector}
    l = length(recombinant)
    from, to = rand(1:l, 2)
    from, to = from > to ? (to, from)  : (from, to)
    l = round(Int,(to - from)/2)
    for i in 0:(l-1)
        swap!(recombinant, from+i, to-i)
    end
    return recombinant
end


# ╔═╡ 477ba1bf-93ef-471f-ba68-17109370637b
md"# Testing the N-Queens Problem"

# ╔═╡ 96ccc389-d124-437d-8681-dabe27c7fdb7
# rosenbrock(x) = (1.0 - x[1])^2 + 100.0 * (x[2] - x[1]^2)^2

# ╔═╡ d38d59c9-f03f-442c-a2d7-1b5cc18349bd
# #selection=rouletteinv sets selection function to the roulette wheel for inverse fitness values 

# # crossover=intermediate a(i) (−0.25;0.25+1) 

# begin 
# ga = GA(populationSize=100, ɛ=0.1, selection=rouletteinv, crossover=intermediate(0.25), mutation=domainrange(fill(0.5,N)))
# 	println(ga)
# end

# ╔═╡ fc1301ba-aea3-4609-9a4d-2fbafda0a5a8
# Vector of N cols filled with numbers from 1:N specifying row position
    function nqueenFunction(queens::Vector{Int})
        n = length(queens)
        fitness = 0
        for i=1:(n-1)
            for j=(i+1):n
                k = abs(queens[i] - queens[j])
                if (j-i) == k || k == 0
                    fitness += 1
                end
                # println("$(i),$(queens[i]) <=> $(j),$(queens[j]) : $(fitness)")
			# println(queens)
            end
        end
	# println("Chest => $queens, Fit : $fitness")
        return fitness
    end

# ╔═╡ f720058a-f8dc-4370-8b22-0e53940a7565
 begin
	N = 8
    P = 50
    generatePositions = ()->collect(1:N)[randperm(N)]
	
@testset "$N-Queens" begin
		
		println("\n\n*****************************")
	println("PopulationSize => $P")
		println("*****************************\n")
		
	    # @show " Testing: GA solution with various mutations"
		
    for muts in [inversion, insertion, swap2, scramble, shifting]
	
		result = Evolutionary.optimize(nqueenFunction,generatePositions,
	
		GA(populationSize = P,selection = susinv,crossover = Evolutionary.PMX, mutation = insertion));
			
		
		print("\nSolution: ")
		show(result.minimizer)
		
		print("\n\nGeneration: ")
        show(result.iterations)
		
		println("\nCrossover Function: OX1")
		println("mutation Function: $muts")
		
		
        @test nqueenFunction(Evolutionary.minimizer(result)) == 0
        @test Evolutionary.iterations(result) < 100 # Test early stopping
			println("*****************************")
		end
    end
end

# ╔═╡ f25fcf8e-2bf0-42ec-a388-bccba73a0e4e
md"# Clearing Terminal"

# ╔═╡ aad5cd62-1481-428b-84e5-4913c6af3cd5
begin
	i = 0
	while i < 20
	println("*")
	i += 1 
 end
end

# ╔═╡ Cell order:
# ╠═f6c1c5e0-ddb7-11eb-3607-ff408fdf4b39
# ╠═be8dda01-1a52-4283-97d9-067bbf0e5bd7
# ╠═5d5d2a16-7fed-4090-b901-ff4bf9476d85
# ╠═4c66368c-6f13-4829-a876-abb0999d313b
# ╠═823a8757-660d-4418-b627-0d6a33f572a0
# ╠═226c1841-7b51-4b08-9c81-70a99dcc1152
# ╠═612c20ee-1292-4bba-94f7-a8a882dd22da
# ╠═ef371103-bf89-483f-9574-efeeb1db295e
# ╠═02545ddc-8723-4fdc-b1e7-af5adf8525b6
# ╠═8cbb1258-d1a1-40a3-a1dc-fd5c5a5ab085
# ╠═d68f7f21-eca9-44ec-9372-405be9affa2f
# ╟─8e831c84-2ec1-4882-8a1d-5c6fda0abfc7
# ╟─a032688a-a209-499d-9951-eab27feca15b
# ╠═b4c44fb9-0fd9-4a2c-87ea-60f3104612f3
# ╠═60106ec6-9b12-457a-8e69-c95a3c4d8cb6
# ╠═faf0eb22-603a-49d1-9561-a6f1eba68fab
# ╟─69251b21-feb8-40c8-b716-23b434f6e439
# ╠═ed694309-def5-4af3-826c-34da628e9b85
# ╟─f0e5cb0e-45f6-46a9-9214-f847255a6212
# ╠═80388176-a6f1-497b-b0a9-b3d926d07879
# ╟─ebf5b893-a9f4-4271-9c69-ab25b0aaa354
# ╠═f73f8a69-35a3-4b96-98e9-3a3cea125342
# ╠═1ec0cade-d3fa-488b-8ddf-4271ffd1ebb2
# ╠═59fa7dbd-6e81-4111-8974-dbdc1c896bd8
# ╠═5066dff5-c2c1-4675-af5b-184455cf7e64
# ╠═47afdf48-0cda-4df5-8fa1-acd18617562b
# ╠═b41e2a4c-32c1-4890-90cd-9fd2344524c5
# ╟─e522ae8a-d721-4d9f-8132-bb99cfab4636
# ╟─b6d16479-6dc2-488a-b5d7-da4a53a57e8f
# ╠═b3d98fe2-b6e2-4939-9c28-95c6478f797f
# ╟─c0ae0d27-5221-473b-ab66-e9361890a29b
# ╠═b9dc04ab-84fa-4213-83a8-27d866abdbc0
# ╟─1e5bb800-fcfd-46fe-b31e-e9da418115b6
# ╠═85ca7d49-85cd-4daa-970a-8f7165187ee4
# ╟─53aea851-68c5-4fce-861e-fee79e011722
# ╠═59c708ad-27e4-4de6-b206-98c4d10cceb7
# ╟─22f5ed9b-b8ff-4eb5-870b-c5690b24f33c
# ╠═e5848996-b623-4122-83c7-bce1059b7854
# ╠═477ba1bf-93ef-471f-ba68-17109370637b
# ╟─96ccc389-d124-437d-8681-dabe27c7fdb7
# ╟─d38d59c9-f03f-442c-a2d7-1b5cc18349bd
# ╠═fc1301ba-aea3-4609-9a4d-2fbafda0a5a8
# ╠═f720058a-f8dc-4370-8b22-0e53940a7565
# ╠═f25fcf8e-2bf0-42ec-a388-bccba73a0e4e
# ╠═aad5cd62-1481-428b-84e5-4913c6af3cd5
