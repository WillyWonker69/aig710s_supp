### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 39e57660-df4c-11eb-2fb0-7faa96b42bdc
begin 
	using Pkg 
	Pkg.activate("Project.toml") 
	using PlutoUI 
end

# ╔═╡ ac59270e-5260-4e6a-b681-084fa4e6596d
using GeoStats: GeoStatsBase

# ╔═╡ ad61e40e-bb2d-4244-9674-a5bc59f0e5b7
using ProgressMeter: @showprogress

# ╔═╡ 1b787e45-2c19-4b3d-8b49-26e351a2a290
# using GeoStatsBase

# ╔═╡ b41a55fc-af89-4a7a-a77a-78ef5f2ebd8d
# abstract type AbstractCSP <: GeoStatsBase.AbstractProblem end;

# ╔═╡ 02a8989e-18f3-4725-ad5b-800d5c000594
md"# CSP
## Solving the Constraint satisfaction Problem"

# ╔═╡ eb043f54-268a-4d04-93e3-03ba93506f11
abstract type AbstractProblem end;

# ╔═╡ 3b20f998-f0e8-43b2-be62-b9389bf22b2c
abstract type AbstractCSP <: AbstractProblem end;

# ╔═╡ 8c862c1f-de8d-4aa7-9091-6d2b49018d2d
@enum D one two three four

# ╔═╡ 2b764ee7-9d37-4857-9f89-60f23000ff77
mutable struct cNetwork_CSPVar
name::String
value::Union{Nothing,D}
forbidden_values::Vector{D}
domain_restriction_count::Int64
end


# ╔═╡ 8f26de29-03f6-45d5-ab80-fa04227a4d9d
struct cNetwork_CSP
vars::Vector{cNetwork_CSPVar}
constraints::Vector{Tuple{cNetwork_CSPVar,cNetwork_CSPVar}}
end


# ╔═╡ 79c0e2a7-ff25-4d77-a254-835dee3558d1
function solve_cNetwork_CSP(CN_CSP::cNetwork_CSP, all_assignments)
		# @show CN_CSP.vars
	for cur_var in CN_CSP.vars
		
		
		if cur_var.domain_restriction_count == 4
		return []
	else
		
next_val = rand(setdiff(Set([one,two,three,four]),Set(cur_var.forbidden_values)))
			
			# @show  (cur_var == x3)
			
# 			if cur_var == x3
# 				cur_var.value = one
# 			else
# 				cur_var.value = next_val
# 			end
			
			cur_var.value = next_val
			
			
			#Back Tracking...
			
#forward checking 
			
			#Check if current assignment violates constraint rule
					
		#else, increase Domain Ristriction and add to forbidden domains
			
			#if True || True... 
	for cur_constraint in CN_CSP.constraints
			if !((cur_constraint[1] == cur_var) || (cur_constraint[2] == cur_var))
				continue #, then Continue
			else
					
	if cur_constraint[1] == cur_var
						
		push!(cur_constraint[2].forbidden_values, next_val)
		cur_constraint[2].domain_restriction_count += 1
						
 			# if the count reaches four you should backtrack
						
						if cur_constraint[2].domain_restriction_count == 4
							return false
						end
							
			else
						
		push!(cur_constraint[1].forbidden_values, next_val)
		cur_constraint[1].domain_restriction_count += 1
		
		  # if the count reaches four you should backtrack
	 if cur_constraint[2].domain_restriction_count == 4
		return false
							
	 end 
						
		end
	end	
				
end

		# add the assignment to all_assignments

		push!(all_assignments, cur_var.name => next_val)
		
	end
end

#Displaing to Terminal

# for a in all_assignments
	# show(a)
# end
			return all_assignments
		return all_assignments
	end

# ╔═╡ 713f2267-c82e-4853-958c-0eaa819332fa
begin
	x1 = cNetwork_CSPVar("X1",nothing, [], 0)
	x2 = cNetwork_CSPVar("X2",nothing, [], 0)

	#unary constraint, X3 = 1 
#(Set, Domain restriction to prevent CSP Alg from assigning any values besides One...

	x3 = cNetwork_CSPVar("X3",nothing, [two,three,four], 0)
	x4 = cNetwork_CSPVar("X4",nothing, [], 0)
	x5 = cNetwork_CSPVar("X5",nothing, [], 0)
	x6 = cNetwork_CSPVar("X6",nothing, [], 0)
	x7 = cNetwork_CSPVar("X7",nothing, [], 0)
end

# ╔═╡ efa6c8de-2e9e-4382-a694-89e14d44caf5
problem = cNetwork_CSP([x1, x2, x3, x4, x5, x6, x7], [(x1,x2), (x1,x3), (x1,x4),
(x1,x5), (x1,x6), (x2,x5), (x3,x4), (x4,x5), (x4,x6), (x5,x6), (x6,x7)])

# ╔═╡ 3f64e4d1-9340-4e9f-b8ad-6e908af7b303


# ╔═╡ 0e8f0f7e-5cf9-4935-b659-dfca08f47486
solve_cNetwork_CSP(problem, [])

# ╔═╡ Cell order:
# ╠═39e57660-df4c-11eb-2fb0-7faa96b42bdc
# ╠═ac59270e-5260-4e6a-b681-084fa4e6596d
# ╠═ad61e40e-bb2d-4244-9674-a5bc59f0e5b7
# ╠═1b787e45-2c19-4b3d-8b49-26e351a2a290
# ╠═b41a55fc-af89-4a7a-a77a-78ef5f2ebd8d
# ╟─02a8989e-18f3-4725-ad5b-800d5c000594
# ╠═eb043f54-268a-4d04-93e3-03ba93506f11
# ╠═3b20f998-f0e8-43b2-be62-b9389bf22b2c
# ╠═8c862c1f-de8d-4aa7-9091-6d2b49018d2d
# ╠═2b764ee7-9d37-4857-9f89-60f23000ff77
# ╠═8f26de29-03f6-45d5-ab80-fa04227a4d9d
# ╠═79c0e2a7-ff25-4d77-a254-835dee3558d1
# ╠═713f2267-c82e-4853-958c-0eaa819332fa
# ╠═efa6c8de-2e9e-4382-a694-89e14d44caf5
# ╠═3f64e4d1-9340-4e9f-b8ad-6e908af7b303
# ╠═0e8f0f7e-5cf9-4935-b659-dfca08f47486
